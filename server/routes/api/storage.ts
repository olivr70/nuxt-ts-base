import { Router } from 'express';

import { asyncHandler } from '../../util';
import { StorageController } from '../../controllers';

const router = Router();

router.get('/', asyncHandler(StorageController.get));
router.get('/:fileName', asyncHandler(StorageController.getByFileName));

export default router;
