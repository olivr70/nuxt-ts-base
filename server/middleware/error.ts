import {
    Request,
    Response,
    NextFunction,
} from 'express';

const consola = require('consola');

export function errorMiddleware(err: any, req: Request, res: Response, next: NextFunction): void {
    if (err) {
        consola.error(err);
        res.status(500);

        if (err instanceof Error) {
            res.json({
                name: err.name,
                message: err.message,
            });
        }
    }

    next();
}
