jest.mock('consola');

const consola = require('consola');

import { errorMiddleware } from './error';

let req: any;
let res: any;
let next: any;

beforeEach(() => {
    req = {};
    res = {
        status: jest.fn(),
        json: jest.fn(),
    };
    next = jest.fn();
});

it('has no error', () => {
    const err = undefined;

    errorMiddleware(err, req, res, next);

    expect(consola.error).not.toBeCalled();
    expect(res.status).not.toBeCalled();
    expect(next).toBeCalled();
});

it('has error', () => {
    const err = new Error('error');

    errorMiddleware(err, req, res, next);

    expect(consola.error).toBeCalled();
    expect(consola.error).toBeCalledWith(err);
    expect(res.status).toBeCalled();
    expect(res.status).toBeCalledWith(500);
    expect(res.json).toBeCalled();
    expect(next).toBeCalled();
});
