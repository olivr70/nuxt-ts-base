import { MarkdownModel } from './markdown.model';

it('should return a list of highlightjs theme', () => {
    const themes = MarkdownModel.getHighlightTheme();

    expect(themes.length).toBeGreaterThan(1);
    expect(themes).not.toBeNull();
    expect(themes).not.toBeUndefined();
});

it('should generate default theme cdn url', () => {
    const CASES = [
        undefined,
        'haha',
    ];

    CASES.forEach((sample) => {
        expect(MarkdownModel.generateThemeCdnUrl(sample))
            .toBe(MarkdownModel.getDefaultThemeCdnUrl());
    });
});

it('should render with default options', () => {
    const result = MarkdownModel.render('#hello');

    expect(result).not.toBeNull();
    expect(result).not.toMatch(/cdnjs.*.css/);
});

it('should render withCss', () => {
    const result = MarkdownModel.render('#hello', {
        withCss: true,
    });

    expect(result).not.toBeNull();
    expect(result).toMatch(/cdnjs.*.css/);
});
