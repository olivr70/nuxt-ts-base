import * as path from 'path';
import * as fs from 'fs';
import * as MarkdownIt from 'markdown-it';

const md = MarkdownIt();
md.use(require('markdown-it-highlightjs'));

const DEFAULT_STYLE = 'vs2015';
const DEFAULT_STYLE_PATH = `highlight.js/styles/${DEFAULT_STYLE}.css`;
const DEFAULT_STYLE_CDN_URL = 'https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.12.0/styles/';

const modulePath = require.resolve(DEFAULT_STYLE_PATH);
const themeDirectoryPath = path.dirname(modulePath);
const themeFiles = fs.readdirSync(themeDirectoryPath)
    .filter((f) => f.match(/.css$/))
    .map((f) => f.replace(/.css$/, ''));

export interface MarkdownOptions {
    withCss?: boolean;
    theme?: string;
}

export const MarkdownModel = {
    render(
        content: string,
        options: MarkdownOptions = {
            withCss: false,
            theme: undefined,
        }): string
    {
        const { withCss, theme } = options;

        let markdown = md.render(content);

        if (withCss) {
            const themeCdnUrl = this.generateThemeCdnUrl(theme);
            const styleSheetLink = this.generateStylesheetLink(themeCdnUrl);
            markdown = styleSheetLink.concat(markdown);
        }

        return markdown;
    },
    generateStylesheetLink(url: string): string {
        return `<link rel="stylesheet" href="${url}" />`;
    },
    generateThemeCdnUrl(theme: string | undefined): string {
        if (theme && themeFiles.includes(theme)) {
            return `${DEFAULT_STYLE_CDN_URL}/${theme}.min.css`;
        }

        return this.getDefaultThemeCdnUrl();
    },
    getDefaultThemeCdnUrl(): string {
        return `${DEFAULT_STYLE_CDN_URL}/${DEFAULT_STYLE}.min.css`;
    },
    getHighlightTheme(): string[] {
        return themeFiles;
    },
};
