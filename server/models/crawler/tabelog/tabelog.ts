
import * as cheerio from 'cheerio';

export interface TabelogCrawlResult {
    name: string;
    url: string;
    address: string;
    link: string;
    holiday: string;
    isRelocated: boolean;
    location: {
        lat: number;
        lng: number;
    };
}

export class TabelogModel {
    static convertToEnglishUrl(url: string): string {
        return url.replace(/.com\/([a-z]{2}\/)?/, '.com/en/');
    }

    static crawl(html: string): TabelogCrawlResult {
        const $ = cheerio.load(html);

        const name = $('a.rd-header__rst-name-main')
            .contents().first().text().trim();

        const url = $('a.rd-header__rst-name-main')
            .attr('href');

        const address = $('p.rd-detail-info__rst-address')
            .contents().first().text().trim();

        const link = $('a[rel=\'nofollow\']')
            .contents().first().text().trim();

        const holiday = $('table.c-table.rd-detail-info')
            .find('tr:contains(\'Shop holidays\')')
            .find('p').text();

        const googleMapLink = $('div.rd-detail-info__rst-map')
            .find('img').first().attr('src');

        const [ _, lat, lng ] = /center=([0-9.]*),([0-9.]*)/.exec(googleMapLink);

        const isRelocated = $('p.rd-header__rst-status-badge').length > 0;

        return {
            name,
            url,
            address,
            link,
            holiday,
            isRelocated,
            location: {
                lat: Number(lat),
                lng: Number(lng),
            },
        };
    }
}
