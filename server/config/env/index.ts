export const Config = {
    CLOUD_STORAGE_FOLDER_NAME: 'timx',
    MONGODB_COLLECTION_LINKS: 'links',
    MONGODB_COLLECTION_RESTAURANT: 'restaurant',
};
