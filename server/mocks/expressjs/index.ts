export const request: any = {
    body: jest.fn(),
    query: jest.fn(),
    params: jest.fn(),
};

export const response: any = {
    set: jest.fn(),
    send: jest.fn(),
    status: jest.fn(),
    json: jest.fn(),
};

export const resetMock = {
    response() {
        response.set.mockReset();
        response.send.mockReset();
        response.status.mockReset();
        response.json.mockReset();
    },
    request() {
        request.body.mockReset();
        request.query.mockReset();
        request.params.mockReset();
    },
    next() {
        next.mockReset();
    },
    all() {
        this.response();
        this.request();
        this.next();
    },
};

export const next = jest.fn();
