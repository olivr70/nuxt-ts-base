import {
    Request,
    Response,
    NextFunction,
} from 'express';

import {
    StorageModel,
    MarkdownModel,
    BlogModel,
} from '../../models';


export const BlogController = {
    async get(req: Request, res: Response, next: NextFunction) {
        const blogs = await BlogModel.get();

        res.json(blogs);
    },
    async post(req: Request, res: Response, next: NextFunction) {
        const { buffer } = req.file;

        const result = await BlogModel.create(req.body, buffer);

        res.json({
            ...result,
        });
    },
    async delete(req: Request, res: Response, next: NextFunction) {
        const { id } = req.body;

        const result = await BlogModel.delete(Number(id));

        res.json({
            id: result,
        });
    },
    async getFromId(req: Request, res: Response, next: NextFunction) {
        const { id } = req.params;

        try {
            const text = await BlogModel.getFile(id);
            const html = MarkdownModel.render(text);

            res.set('Content-Type', 'text/html');
            res.send(html);
        } catch (e) {
            next(new Error(`Blog ${id} not found`));
        }
    },
    async getFromFileName(req: Request, res: Response, next: NextFunction) {
        const { fileName } = req.params;

        const [ result ] = await StorageModel.getFile(fileName);

        const html = MarkdownModel.render(result.toString());

        res.set('Content-Type', 'text/html');
        res.send(html);
    },
};
