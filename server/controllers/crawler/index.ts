import {
    Request,
    Response,
    NextFunction,
} from 'express';

import {
    CrawlerHandlerMap,
} from './type';

import {
    TabelogCrawler,
} from './tabelog';

export * from './tabelog';

const CrawlerHandler: CrawlerHandlerMap = {
    tabelog: TabelogCrawler,
};

export const CrawlerController = {
    async get(req: Request, res: Response, next: NextFunction) {
        const types = Object.keys(CrawlerHandler);

        res.json({
            types,
        });
    },
    async post(req: Request, res: Response, next: NextFunction) {
        const { url, type } = req.body;

        if (CrawlerHandler[type]) {
            const result = await CrawlerHandler[type](url);
            res.json(result);

            return;
        }

        next(new Error(`Crawler type (${type}) is not supported`));
    },
    tabelog: {
        async get(req: Request, res: Response, next: NextFunction) {
            const { url } = req.params;

            const result = await CrawlerHandler.tabelog(url);
            res.json(result);
        },
    },
};
