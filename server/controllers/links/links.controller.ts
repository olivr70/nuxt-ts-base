import {
    Request,
    Response,
    NextFunction,
} from 'express';

import { LinksModel } from '../../models';

export const LinksController = {
    async get(req: Request, res: Response, next: NextFunction): Promise<void> {
        const result = await LinksModel.get();

        res.json(result);
    },
    async getById(req: Request, res: Response, next: NextFunction): Promise<void> {
        const { id } = req.params;

        const result = await LinksModel.getById(id);

        res.json(result);
    },
    async deleteById(req: Request, res: Response, next: NextFunction): Promise<void> {
        const { id } = req.params;

        const error = await LinksModel.deleteById(id);

        if (error) {
            next(error);
            return;
        }

        res.json({
            id,
        });
    },
    async post(req: Request, res: Response, next: NextFunction): Promise<void> {
        const { category, title, url } = req.body;

        if (!category
            && !title
            && !url)
        {
            throw new Error('Not correct body field, please provide category and title');
        }

        const [ err, result ] = await LinksModel.create({
            category,
            title,
            url,
        });

        if (err) {
            next(err);
            return;
        }

        res.json(result);
    },
};
