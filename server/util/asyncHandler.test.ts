import { asyncHandler } from './asyncHandler';

it('should not throw an error', async () => {
    expect.assertions(1);

    const res: any = jest.fn();
    const req: any = jest.fn();
    const next = jest.fn();

    await asyncHandler(async () => {
        throw new Error();
    })(req, res, next);

    expect(next).toBeCalled();
});
