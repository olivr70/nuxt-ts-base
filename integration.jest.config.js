const config = require('./jest.config');

config.testPathIgnorePatterns = config.testPathIgnorePatterns
    .filter((x) => x !== 'integration/');

config.watchPathIgnorePatterns = config.watchPathIgnorePatterns
    .filter((x) => x !== 'integration/');

module.exports = config;
