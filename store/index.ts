import Vuex from 'vuex';

import * as restaurant from './restaurant';
import * as location from './location';

const createStore = () => {
    return new Vuex.Store({
        modules: {
            restaurant,
            location,
        },
    });
};

export default createStore;
