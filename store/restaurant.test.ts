import { getters } from './restaurant';
import { RestaurantState } from './types';

describe('getters', () => {
    describe('getRestaurantByCategories', () => {
        it('should sort restaurants based on countries', () => {
            const EXPECTED = [
                {
                    key: 'JP',
                    restaurants: [
                        {
                            name: 'Mimatsu (美松)',
                            url: 'http://tabelog.com/en/tokyo/A1305/A130501/13003920/',
                            address: '2-18-1 Ikebukuro Toshima Tokyo',
                            link: '',
                            holiday: '日曜・祝日',
                            isRelocated: false,
                            rating: 7,
                            country: 'JP',
                            location: {
                                lat: 35.73213343136423,
                                lng: 139.70608419649045,
                            },
                        },
                        {
                            name: 'Soranoi Ramen (ソラノイロ)',
                            url: 'https://tabelog.com/en/tokyo/A1308/A130803/13126816/',
                            address: '1-3-10 Hirakawacho Chiyoda Tokyo',
                            link: '',
                            holiday: '日曜日',
                            isRelocated: false,
                            rating: 6,
                            country: 'JP',
                            location: {
                                lat: 35.68305287454349,
                                lng: 139.73942535393576,
                            },
                        },
                    ],
                },
                {
                    key: 'US',
                    restaurants: [
                        {
                            name: 'Red Lobster',
                            url: 'https://tabelog.com/en/tokyo/A1313/A131306/13017921/',
                            address: '1-7-1 Daiba Minato Tokyo',
                            link: '',
                            holiday: '不定休（アクアシティお台場に準ずる）',
                            isRelocated: false,
                            rating: 8,
                            country: 'US',
                            location: {
                                lat: 35.62762773355753,
                                lng: 139.77362478777312,
                            },
                        },
                    ],
                },
            ];

            const state: RestaurantState = {
                restaurants: [
                    {
                        name: 'Mimatsu (美松)',
                        url: 'http://tabelog.com/en/tokyo/A1305/A130501/13003920/',
                        address: '2-18-1 Ikebukuro Toshima Tokyo',
                        link: '',
                        holiday: '日曜・祝日',
                        isRelocated: false,
                        rating: 7,
                        country: 'JP',
                        location: {
                            lat: 35.73213343136423,
                            lng: 139.70608419649045,
                        },
                    },
                    {
                        name: 'Soranoi Ramen (ソラノイロ)',
                        url: 'https://tabelog.com/en/tokyo/A1308/A130803/13126816/',
                        address: '1-3-10 Hirakawacho Chiyoda Tokyo',
                        link: '',
                        holiday: '日曜日',
                        isRelocated: false,
                        rating: 6,
                        country: 'JP',
                        location: {
                            lat: 35.68305287454349,
                            lng: 139.73942535393576,
                        },
                    },
                    {
                        name: 'Red Lobster',
                        url: 'https://tabelog.com/en/tokyo/A1313/A131306/13017921/',
                        address: '1-7-1 Daiba Minato Tokyo',
                        link: '',
                        holiday: '不定休（アクアシティお台場に準ずる）',
                        isRelocated: false,
                        rating: 8,
                        country: 'US',
                        location: {
                            lat: 35.62762773355753,
                            lng: 139.77362478777312,
                        },
                    },
                ],
            };

            expect(getters.getRestaurantByCategories(state)).toStrictEqual(EXPECTED);
        });
    });
});
