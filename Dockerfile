FROM node:10

LABEL name="timx-site"
LABEL version="1.1"
LABEL maintainer="hosweetim@gmail.com"

WORKDIR /usr/timx

COPY package*.json ./
RUN npm install

COPY . .
RUN npm run build

EXPOSE 8080

CMD ["npm", "start"]